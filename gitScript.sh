#!/bin/sh

cd "$(dirname "$0")"
GIT=`which git`
${GIT} init
${GIT} add .
${GIT} commit -m "First commit"
${GIT} push $1 master