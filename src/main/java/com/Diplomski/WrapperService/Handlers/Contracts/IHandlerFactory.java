package com.Diplomski.WrapperService.Handlers.Contracts;

import com.Diplomski.WrapperService.Handlers.Exceptions.HandlerNotFoundException;

public interface IHandlerFactory {

    IHandler getHandler(HandlerTypes handlerType);
}
