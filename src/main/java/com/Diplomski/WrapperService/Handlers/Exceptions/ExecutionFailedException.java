package com.Diplomski.WrapperService.Handlers.Exceptions;

public class ExecutionFailedException extends RuntimeException {

    public ExecutionFailedException(String s) {
        super(s);
    }
}
