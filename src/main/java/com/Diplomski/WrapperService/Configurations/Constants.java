package com.Diplomski.WrapperService.Configurations;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

    public static final String EXECUTE_PATH = "/execute";
    public static final String CODE_PATH = "/code";
}
