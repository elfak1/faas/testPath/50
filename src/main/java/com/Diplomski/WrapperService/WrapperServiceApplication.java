package com.Diplomski.WrapperService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WrapperServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(WrapperServiceApplication.class, args);
	}
}
