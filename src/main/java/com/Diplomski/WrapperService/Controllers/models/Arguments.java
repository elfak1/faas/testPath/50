package com.Diplomski.WrapperService.Controllers.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.List;
import lombok.Builder;
import lombok.Getter;

@Getter
@JsonDeserialize(builder = Arguments.argumentsBuilder.class)
@Builder(builderClassName = "argumentsBuilder", setterPrefix = "with")
public class Arguments {
    private final List<Object> arguments;
}
