package com.Diplomski.WrapperService.Controllers.models.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponse {

    private Integer status = 200;
    private String error = "";
    private Integer errorCode = 0;
    private String message = "Success";

    public BaseResponse(Integer statusCode) {
        this.status = statusCode;
    }

    public BaseResponse(String message) {
        this.message = message;
    }

    public BaseResponse(Integer statusCode, String error, String message) {
        this.status = statusCode;
        this.error = error;
        this.message = message;
    }
}
