package com.Diplomski.WrapperService.Controllers;

import com.Diplomski.WrapperService.Controllers.models.Arguments;
import com.Diplomski.WrapperService.Controllers.models.response.GenericDataResponse;
import com.Diplomski.WrapperService.Handlers.Contracts.IHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static com.Diplomski.WrapperService.Configurations.Constants.*;

@RestController
@RequiredArgsConstructor
public class Gateway {

    final private IHandler handler;

    @PostMapping(EXECUTE_PATH)
    public GenericDataResponse<Object> execute(@RequestBody Arguments arguments) {
        final Object executionResponse = handler.execute(arguments.getArguments());
        return new GenericDataResponse<>(executionResponse);
    }

    @GetMapping(CODE_PATH)
    public ResponseEntity<Object> getUserCode() {
        return new ResponseEntity<>(handler.getUserCode(), HttpStatus.OK);
    }
}
